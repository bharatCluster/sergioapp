import React, { useContext } from 'react';
import {
  Container, NavBar, TextField,
} from 'components';
import PropTypes from 'prop-types';
import FormContainer from './FormContainer';

const SignUp = ({ navigation }) => {
  return (
    <Container asGradient>
      <NavBar
        onLeftIconPress={() => navigation.navigate('Login') } leftIconName="chevron-left"
      />
      <FormContainer
        title="Create an Account"
        subtitle="Join today along with million other users to the most exclusive e-commerce platform ever!"
        buttonLabel="Sign Up"
        onSubmit={() => {navigation.navigate('Login')}}
      >
        <TextField label="Name" />
        <TextField label="Phone number" />
        <TextField label="Email address" />
        <TextField label="Password" secureTextEntry />
        <TextField label="Confirm Password" secureTextEntry />
      </FormContainer>
    </Container>
  );
};

SignUp.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default SignUp;
