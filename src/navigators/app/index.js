import React, { useContext } from 'react';
import { createNativeStackNavigator } from 'react-native-screens/native-stack';
import { enableScreens } from 'react-native-screens';

import SignUp from '../../containers/Auth/SignUp';
import Login from '../../containers/Auth/Login';
import ForgotPassword from '../../containers/Auth/ForgotPassword';

enableScreens();
const Stack = createNativeStackNavigator();

const App = () => {

  return (
    <>
        <>
          <Stack.Navigator
            // mode="modal"
            screenOptions={{
              headerShown: false,
            }}
          >
            <Stack.Screen name="SignUp" component={SignUp} />

            <Stack.Screen name="Login" component={Login} />
            <Stack.Screen name="ForgotPassword" component={ForgotPassword} />
       
          </Stack.Navigator>
        </>
    </>
  );
};

export default App;
