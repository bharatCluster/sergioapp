import 'react-native-gesture-handler';
import React, { useEffect } from 'react';
import SplashScreen from 'react-native-splash-screen';
import { LogBox,View,Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import AppNavigator from 'navigators/app';

LogBox.ignoreLogs(['VirtualizedLists', 'componentWillReceiveProps']);

const App = () => {
  useEffect(() => {
    SplashScreen.hide();
  }, []);

  return (
    <NavigationContainer>
        <AppNavigator />
      </NavigationContainer>
  );
};

export default App;
